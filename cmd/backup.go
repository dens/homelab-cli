/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

// backupCmd represents the backup command
var backupCmd = &cobra.Command{
	Use:   "backup",
	Short: "Wrapper for rsync backup",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Backup called")

		source, sErr := cmd.Flags().GetString("source")
		destination, dErr := cmd.Flags().GetString("destination")
		compress, _ := cmd.Flags().GetString("compress")

		if sErr != nil || dErr != nil {
			fmt.Printf("Problem with source/dest flags:")
		}

		fmt.Printf("Source is %s\nDestination is %s\n", source, destination)
		shell := exec.Command("/usr/bin/rsync", "-aPn", "--stats", source, destination)

		// direct STDERR/STDOUT to OS
		shell.Stdout = os.Stdout
		shell.Stderr = os.Stderr

		fmt.Printf("Running rsync with args %v.\n", shell.Args)
		err := shell.Run()
		if err != nil {
			fmt.Printf("problem with running backup command: %s", err)
		}

		destPath, err := os.Stat(destination)

		if destPath.IsDir() = false && compress {

		}
			
			
		
	},
}

func init() {
	rootCmd.AddCommand(backupCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// backupCmd.PersistentFlags().String("source", "/Users/cdelcourt/Code/home/backup-testing", "Source for backup")
	// backupCmd.PersistentFlags().String("dest", "/tmp/backup.tar.gz", "Destination for backup")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	backupCmd.Flags().String("source", "/Users/cdelcourt/Code/home/backup-testing/", "Source path for backup")
	backupCmd.Flags().String("destination", "/tmp/backup", "Destination path for backup")
	backupCmd.Flags().Bool("compress", true, "Enable compressed backup")
}
