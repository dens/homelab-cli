/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
)

type Config struct {
	// AwsSecretAccessKey & co are credentials for an AWS client
	AwsSecretAccessKey string `secret:"IAM_AWS_SECRET_ACCESS_KEY" env:"AWS_SECRET_ACCESS_KEY" default:""`
	AwsAccessKeyID     string `secret:"IAM_AWS_ACCESS_KEY_ID" env:"AWS_ACCESS_KEY_ID" default:""`
	AwsSessionToken    string `secret:"IAM_AWS_SESSION_TOKEN" env:"AWS_SESSION_TOKEN" default:""`

	AwsCredentials aws.CredentialsProvider

	AWSRegion string `env:"AWS_REGION" default:"ap-southeast-2"`
}

// uploadCmd represents the upload command
var uploadCmd = &cobra.Command{
	Use:   "upload",
	Short: "Uploads files from a path to AWS S3",
	Long: `This will take a source path and destination bucket for uploading
	files (e.g. backups) to AWS S3.`,
	Run: func(cmd *cobra.Command, args []string) {
		conf := Load(cmd.Context())
		creds, err := getAwsCredentials(cmd.Context(), conf)
		conf.AwsCredentials = creds
		if err != nil {
			fmt.Errorf("Error loading AWS credentials! %w", err)
		}

	},
}

func init() {
	rootCmd.AddCommand(uploadCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// uploadCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	uploadCmd.Flags().BoolP("upload", "u", false, "Help message for upload")
	uploadCmd.Flags().String("source", "", "Source path for uploads")
	uploadCmd.Flags().String("bucket", "", "Bucket to upload source files to")
}

func getAwsCredentials(ctx context.Context, config Config) (aws.CredentialsProvider, error) {
	if config.AwsAccessKeyID == "" {
		return getDefaultAWSCredentials(ctx)
	}
	return credentials.StaticCredentialsProvider{Value: aws.Credentials{
		AccessKeyID:     config.AwsAccessKeyID,
		SecretAccessKey: config.AwsSecretAccessKey,
		SessionToken:    config.AwsSessionToken,
	}}, nil
}

func getDefaultAWSCredentials(ctx context.Context) (aws.CredentialsProvider, error) {
	awsConfig, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load default AWS credentials")
	}
	return awsConfig.Credentials, nil
}

func Load(ctx context.Context) Config {
	conf := Config{}

	conf.AwsCredentials, _ = getAwsCredentials(ctx, conf)
	// if err != nil {
	// 	return Config{}
	// }

	return conf
}
